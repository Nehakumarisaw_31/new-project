package com.dao;


import com.model.Registration;

public interface RegisterDaoIntf {

	void saveDetails(Registration registration);

	int checkUsers(Registration registration);

	
}
