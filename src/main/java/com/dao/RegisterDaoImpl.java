package com.dao;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Login;
import com.model.Registration;




@Repository
public class RegisterDaoImpl implements RegisterDaoIntf {
	@Autowired
	SessionFactory sessionFactory;
	public void saveDetails(Registration registration) {
		sessionFactory.openSession().save(registration);
	}
	
	public int checkUsers(Registration registration) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from faculty_tbl where firstName=:id and password=:password").addEntity(Registration.class);
		query.setParameter("id",registration.getFirstName());
		query.setParameter("password",registration.getPassword());
		if(query.uniqueResult()==null) {
			return 0;
		}else {
			return 1;
		}
		
		
	}

	
	
	

}
