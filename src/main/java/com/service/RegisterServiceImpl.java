package com.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.RegisterDaoIntf;
import com.model.Registration;
@Service
@Transactional
public class RegisterServiceImpl implements RegisterServiceIntf {
	@Autowired
	RegisterDaoIntf dao;

	public void saveDetails(Registration registration) {
		dao.saveDetails(registration);
		
	}


	public int checkUser(Registration registration) {
		int result=dao.checkUsers(registration);
		return result;
	}
		

}
