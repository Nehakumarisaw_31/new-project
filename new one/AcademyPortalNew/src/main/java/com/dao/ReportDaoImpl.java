package com.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Batch;
import com.model.Report;


@Repository
public class ReportDaoImpl implements ReportDaoIntf{
	@Autowired
	SessionFactory sessionFactory;
	public List<Batch> getBatchId() {
		
		List<Batch> list = sessionFactory.openSession().createQuery("from Batch").list();
		return list;
	}

	public void calculateCredit(Report report) {
		float point;
		int val=0;
		//report.setBatchId(1);
		if(report.getRating()==5 && report.getStatusOfReportSubmission().equals("yes") ) {
			report.setFacultyCreditPoint(10);
		}else if(report.getRating()==5 && report.getStatusOfReportSubmission().equals("yes"))
		if(report.getStatusOfReportSubmission().equals("yes")) {
			val=5;
		}
		point=(report.getRating()+report.getNoOfWorkingDays()+val)/3;
		report.setFacultyCreditPoint(point);
		System.out.println(report.getBatchId()+" "+report.getRating()+" "+report.getNoOfWorkingDays()+" "+report.getStatusOfReportSubmission()+" "+report.getFacultyCreditPoint());
		sessionFactory.openSession().createSQLQuery("insert into report_tbl(id,batchId,rating,noOfWorkingDays,statusOfReportSubmission,facultyCreditPoint) values(4,"+report.getBatchId()+","+report.getRating()+","+report.getNoOfWorkingDays()+",'"+report.getStatusOfReportSubmission()+"',"+report.getFacultyCreditPoint()+")").executeUpdate();
		
		//sessionFactory.openSession().save(report);
		/*float point;
		int val=0;
		Query query=sessionFactory.openSession().createSQLQuery("update report_tbl set facultyCreditPoint=:point where batchId=:id");
		query.setParameter("id",report.getBatchId());
		if(report.getRating()==5 && report.getStatusOfReportSubmission().equals("yes") ) {
			report.setFacultyCreditPoint(10);
		}else if(report.getRating()==5 && report.getStatusOfReportSubmission().equals("yes"))
		if(report.getStatusOfReportSubmission().equals("yes")) {
			val=5;
		}
		point=(report.getRating()+report.getNoOfWorkingDays()+val)/3;
		query.setParameter("point",point);
		query.executeUpdate();*/
	}

}
