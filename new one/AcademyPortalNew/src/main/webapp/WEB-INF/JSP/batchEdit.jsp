<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Batch Details</title>
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
<style type="text/css">
.topnav {
  overflow: hidden;
  background-color: #3C4551;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
  font-weight: bold;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #E15050;
  color: white;
}
.table1{
}
</style>
</head>
<body>

<div class="topnav">
		<a class="active" href="#">Academy Portal</a>
		
		
		<div class="topnav-right">
			<a href="index.jsp">Home</a>
		</div>
	</div>
	
	<br><br><br><br><br><br>
	<h1>Edit Batch</h1>
<form:form action="/AcademyPortal/batchSave">

<form:hidden path="batchId" />

<form:label path="batchName">Batch Name</form:label><br/>
<form:input path="batchName"/><br/>
<form:label path="trainerName">Trainer Name</form:label><br/>
<form:input path="trainerName" /><br/>
<form:label path="noOfTrainees">noOfTrainees</form:label><br/>
<form:input path="noOfTrainees" /><br/>
Domain:<form:select path="domain">
    <form:option value="-" label="--Select Domain--"/>
    <form:option value="java" label="Java"/>
    <form:option value="linux" label="Linux"/>
    <form:option value="Dotnet" label="DotNet"/>
    <form:option value="python" label="Python"/>
    <form:option value="Aws" label="Aws"/>
    </form:select><br/>
batchStartDate:<form:input type="date" path= "batchStartDate"/><br/>
batchEndDate:<form:input type="date" path= "batchEndDate"/><br/>
<input type="submit" value="Submit" class="btn btn-primary">
</form:form>
</body>
</html> 