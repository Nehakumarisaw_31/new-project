package com.dao;

import java.util.List;

import com.model.Batch;

public interface BatchDaoIntf {

	void saveBatch(Batch batch);

	List<Batch> getUsers();

	Batch getDetails(int id);

	void edit(Batch batch);

	void delete(int id);

}
