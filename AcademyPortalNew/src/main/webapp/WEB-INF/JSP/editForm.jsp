<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Batch Update</title>
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
<style type="text/css">
.tablepro
{border-collapse:collapse;
margin-left: auto;
margin-right: auto;
}
</style>
</head>
<body>
<div class="topnav">
		<a class="active" href="#">Academy Portal</a>
		
		
		<div class="topnav-right">
			<a href="index.jsp">Home</a>
		</div>
	</div>
	<br />
	<br />
	<br />
	<div align="center"> 
<form:form action="/SpringMVC/updateSave" style="max-width: 450px; margin:0 auto;">    
<table border="0">
<tr>
				     
					<td colspan="2" align="center"><h2 class="heading1"> Update Faculty Details</h2></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><p class="heading3">Fill the Faculty Details</p></td>
				</tr>
				<tr>
				<td><form:hidden path="associateId" /> </td>
				</tr>
 
<tr>
				<td><form:label path="firstName">First Name</form:label><br/></td>
				<td><form:input path="firstName"/><br/></td>
				</tr>
				
				<tr>
				<td><form:label path="lastName">Last Name</form:label><br/></td>
				<td><form:input path="lastName" /><br/></td>
				</tr>
				
				<tr>
				<td><form:label path="age">Age</form:label><br/></td>
				<td><form:input path="age" /><br/></td>
				</tr>
				
				<tr>
				<td><form:label path="gender">Gender</form:label></td>
				<td><form:radiobutton path="gender" value="Male"/>Male</td>
				<td><form:radiobutton path="gender" value="Female"/>Female <br/></td>
				</tr>
				
				<tr>
				<td><form:label path="contactNumber">Contact Number</form:label><br/></td>
				<td><form:input path="contactNumber" /><br/></td>
				</tr>
				
				
				<tr>
				<td><form:label path="emailId">Email Id</form:label><br/></td>
				<td><form:input type="email" path="emailId"/><br/> </td>
				</tr>
				
				<tr>
                 <td colspan="2" align="center"><input type="submit" value="Submit" class="btn btn-primary"/></td>
                 </tr>
				 </table>
</form:form>    </div>
</body>
</html>