<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ReportSubmission</title>
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
</head>
<body>
<body>
<div class="topnav">
		<a class="active" href="#">Academy Portal</a>
		
		
		<div class="topnav-right">
			<a href="index.jsp">Home</a>
		</div>
	</div>
	<br />
	<br />
	<br /><br /><br />
	<br />
	<br />
	<div align="center">
	<form:form action="/SpringMVC/credit" modelAttribute="report">
	<table border="0">
			<tr>
				     
					<td colspan="2" align="center"><h2 class="heading1">Report Management </h2></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><p class="heading3">Fill the Report Details</p></td>
				</tr>
				<tr><td><form:label path="batchId">Batch Id</form:label><br/></td>
				<td><form:input path="batchId" value="${id}"/><br/></td>
				</tr>
				
				<tr>
				<td><form:label path="rating">Rating</form:label><br/></td>
				<td><form:input path="rating"/><br/></td>
				</tr>
				
	            <tr>
				<td><form:label path="noOfWorkingDays">No Of Working Days</form:label><br/></td>
				<td><form:input path="noOfWorkingDays"/><br/></td>
				</tr>
				
				<tr>
				<td><form:label path="statusOfReportSubmission">Status Of Report </form:label><br/></td>
				<td><form:input path="statusOfReportSubmission"/><br/></td>
				</tr>

                 <tr>
                 <td colspan="2" align="center"><input type="submit" value="Submit" class="btn btn-primary"/></td>
                 </tr>
                







<%-- <form:checkbox path="statusOfReportSubmission"/>Yes
<form:checkbox path="statusOfReportSubmission"/>No<br/> --%>
<!-- <input type="submit" value="Submit"> --></table>
</form:form></div>
</body>
</html>